package com.example.gcd;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import android.annotation.SuppressLint;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.NumberPicker;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity
{
    private FrameLayout[] matrixSlots;
    private NumberPicker rows;
    private NumberPicker columns;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /* Attribute Declarations */

        final short MIN = 1;      // Minimum matrix size
        final short MAX = 4;      // Maximum matrix size
        final short DEFAULT = 2;  // Default selection for rows & columns

        rows = (NumberPicker)findViewById(R.id.rows);
        columns = (NumberPicker)findViewById(R.id.columns);

        Button compute = (Button)findViewById(R.id.compute);
        EditText firstInteger = (EditText)findViewById(R.id.firstInteger);
        EditText secondInteger = (EditText)findViewById(R.id.secondInteger);
        TextView output = (TextView)findViewById(R.id.output);
        GridView matrixGrid = (GridView)findViewById(R.id.matrixGenerator);

        /* Set the properties to the row and column selectors */
        rows.setMinValue(MIN);
        rows.setMaxValue(MAX);
        rows.setValue(DEFAULT);

        columns.setMinValue(MIN);
        columns.setMaxValue(MAX);
        columns.setValue(DEFAULT);

        /* Generate and populate matrix */
        createMatrix(matrixGrid);

        /* These event listeners update the matrix if the user changes the number
        * of rows and columns */

        rows.setOnValueChangedListener(new NumberPicker.OnValueChangeListener()
        {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal)
            {
                createMatrix(matrixGrid);
            }
        });

        columns.setOnValueChangedListener(new NumberPicker.OnValueChangeListener()
        {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal)
            {
                createMatrix(matrixGrid);
            }
        });

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, "MyChannel")
                .setSmallIcon(R.drawable.ic_launcher_background)
                .setContentTitle("GCD Computed!")
                .setContentText("The GCD is: x")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);
        compute.setOnClickListener(new View.OnClickListener()

        {
            int result = -1;

            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View view)
            {
                try
                {
                    int a = Integer.parseInt(firstInteger.getText().toString());
                    int b = Integer.parseInt(secondInteger.getText().toString());
                    result = gcdr(a, b);
                }
                catch (NumberFormatException error)
                {
                    output.setText(R.string.inputError);
                }
                finally
                {
                    NotificationManagerCompat notificationManager = NotificationManagerCompat.from(MainActivity.this);

                    // notificationId is a unique int for each notification that you must define
                    notificationManager.notify(1, builder.build());

                    output.setText(Integer.toString(result));
                }
            }
        });
    }

    private void createNotificationChannel()
    {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.ChannelComputed);
            String description = getString(R.string.ChannelComputed);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("My Channel", name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }


    /**
     * Generates the matrix slots and populates the GridView on the main activity
     *
     * Online references: https://abhiandroid.com/ui/custom-arrayadapter-tutorial-example.html
     *
     * @param matrixGrid The gridView with the R-id matrixGrid
     */
    public void createMatrix(GridView matrixGrid)
    {
        short totalSlots = (short)(rows.getValue() * columns.getValue());
        FrameLayout[] matrixSlots = new FrameLayout[totalSlots];
        InputAdapter adapter = new InputAdapter(this, R.layout.matrix_input);

        matrixGrid.setNumColumns(columns.getValue());
        for (int iterator = 0; iterator < totalSlots; iterator++)
        {
            matrixSlots[iterator] = new FrameLayout(this);
            adapter.add(matrixSlots[iterator]);
        }
        matrixGrid.setAdapter(adapter);
    }

    /**
     * Iterative implementation of Euclideans algorithm
     *
     * @param a the first integer
     * @param b the second integer
     * @return the greatest common factor of a and b
     */
    private int gcd(int a, int b)
    {
        int firstVal = a;
        int secondVal = b;

        int remainder = firstVal % secondVal;
        firstVal = secondVal;

        while (remainder != 0)
        {
            secondVal = remainder;
            remainder = firstVal % secondVal;
            firstVal = secondVal;
        }

        return secondVal;
    }

    /**
     * Recursive implementation of Euclideans algorithm
     *
     * @param a the first integer
     * @param b the second integer
     * @return the greatest common factor of a and b.
     */
    private int gcdr(int a, int b)
    {
        int remainder = a % b;
        a = b;

        if (remainder == 0)
        {
            return a;
        }

        return gcdr(b, remainder);
    }
}
package com.example.gcd;

import android.content.ClipData;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;

public class InputAdapter extends ArrayAdapter<FrameLayout>
{

    public InputAdapter(@NonNull Context context, int resource)
    {
        super(context, resource);

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.matrix_input, null);
        EditText input = convertView.findViewById(R.id.editTextNumber2);

        return convertView;
    }
}
